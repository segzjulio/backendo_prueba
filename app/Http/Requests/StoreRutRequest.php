<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Freshwork\ChileanBundle\Rut;
use App\Rules\RutUnique;

class StoreRutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'rut' => 'required|string|max:15|cl_rut|unique:ruts,rut'
            'rut' => ['required', 'string', 'max:15', new RutUnique, 'cl_rut'],
        ];
    }
}
