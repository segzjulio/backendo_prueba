<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Freshwork\ChileanBundle\Rut;
use App\Models\Ruts;
use App\Http\Requests\StoreRutRequest;

class RutController extends Controller
{
    public function __construct() {
        $this->middleware('formated_rut')->only(['save', 'update']);
    }

    public function get($rut){
        $formatRut = Rut::parse($rut)->format(Rut::FORMAT_COMPLETE);
        $user = Ruts::select('id', 'rut')->where('rut', $formatRut)->firstOrFail();
        return response()->json($user);
    }

    public function getAll(){
        $user = Ruts::select('id', 'rut')->orderBy('id', 'desc')->get();

        return response()->json($user);
    }

    public function save(StoreRutRequest $request)
    {
        $rut = new Ruts();
        
        $rut->rut = $request->rut;
        $rut->save();
        
        return response()->json([
            'rut_ingresado' => $request->rut,
            'rut_formateado' => $rut->rut,
        ]);
    }
    
    public function update(Request $request, Ruts $rut){
        $request->validate([
            'rut' => ['required', 'string', 'max:15', 'cl_rut'],
        ]);

        $rut_anterior = $rut->rut;
        $rut->rut = $request->rut;
        $rut->save();

        return response()->json([
            'rut_ingresado' => $request->rut,
            'rut_anterior' => $rut_anterior,
            'rut_actualizado' => $rut->rut,
        ]);
    }

    public function delete(Ruts $rut){
        $rut->delete();

        return response()->json([
            'rut_eliminado' => $rut->rut,
        ]);
    }
}
