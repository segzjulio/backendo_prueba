<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Freshwork\ChileanBundle\Rut;

class FormatedRut
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $r = Rut::parse($request->rut)->quiet()->format(Rut::FORMAT_COMPLETE);
        
        $request->replace([
            'rut' => $r
        ]);
        return $next($request);
    }
}
