<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Freshwork\ChileanBundle\Rut;

class Ruts extends Model
{
    use HasFactory;

    protected $table = 'ruts';

    protected $fillable = [
        'rut'
    ];

    // public function setRutAttribute($value)
    // {
    //     $this->attributes['rut'] = Rut::parse($value)->format(Rut::FORMAT_COMPLETE);
    // }

}
