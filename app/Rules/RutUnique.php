<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Freshwork\ChileanBundle\Rut;
use App\Models\Ruts;

class RutUnique implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $formatRut = Rut::parse($value)->format(Rut::FORMAT_COMPLETE);
        $user = Ruts::select('id', 'rut')->where('rut', $formatRut)->first();
        
        return $user == null; 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El rut ingresado ya se encuentra guardado.';
    }
}
