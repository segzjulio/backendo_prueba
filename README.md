************************************************************************************************************
*  Pasos a seguir para instalar el proyecto:                                                               *
*      1.-Descargar e instalar Laragon.                                                                    *
*      2.-Clonar este repositorio del proyecto en el directorio C:/laragon/www/                            *
*      3.-Abrir una consola alojada en la carpeta del proyecto y ejecutar el comando "composer install".   *
*      4.-Recargar apache si es necesario.                                                                 *
*      5.-Crear BD en mysql.                                                                               *
*      6.-copiar informacion de .env.example en el archivo .env                                            *
*      7.-Ejecutar php artisan key:generate.                                                               *
*      8.-Ejecutar php artisan migrate (Para la creacion de las tablas en la BD).                          *
*      9.-Se pueden crear rut aleatorios ocupando la clase factory                                         *
************************************************************************************************************

