<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutController;
use App\Models\Ruts;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(RutController::class)->group(function() {
    Route::get('rut/get/{rut}', 'get');
    Route::get('rut/getAll', 'getAll');
    Route::post('rut/save', 'save');
    Route::put('/rut/update/{rut}', 'update');
    Route::delete('/rut/delete/{rut}', 'delete');
});
