<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Freshwork\ChileanBundle\Rut;

class RutsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'rut' => $this->getRandomRut(),
        ];
    }

    protected function getRandomRut(){
        //generate random number between 1.000.000 and 25.000.000
        $random_number = rand(1000000, 25000000);

        //We create a new RUT wihtout verification number (the second paramenter of Rut constructor)
        $rut = new Rut($random_number);

        return $rut->fix()->format();
    }
}
